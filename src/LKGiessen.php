<?php

namespace App;

class LKGiessen
{
  public string $URI = 'https://corona.lkgi.de';

  public function getWebpageContent(): string
  {
	$filename = __DIR__ . '/../resources/lgki.html';
//    if (file_exists($filename)) {
//      $content = file_get_contents($filename);
//    } else {
	$content = file_get_contents($this->URI);
//      file_put_contents($filename, $content);
//    }

	return $content;
  }

  /**
   * Get inhabitants per municipality
   *
   * @return array Inhabitants per municipality
   */
  private function getInhabitants(): array {
	return json_decode(file_get_contents(__DIR__ . '/../resources/inhabitants.json'), true);
  }

  /**
   * Get cases and incidence per municipality
   * @return array{array{kommune: string, slug: string, inzidenz: float, faelle: int}}
   */
  public function getCasesAndIncidencePerMunicipality(): array
  {
	$content = $this->getWebpageContent();

	// Get data JSON from within script tags of the webpage
	$regex = '/render_data:\s(\{.*\}), engine:/';
	preg_match_all($regex, $content, $matches);
	$output = [];
	$inhabitants = $this->getInhabitants();
	foreach ($matches[1] as $json) {
	  $obj = json_decode($json);
	  $series = $obj?->options?->series;
	  $labels = $obj?->options?->xAxis?->categories;
	  $slugs = array_map(fn($label) => Helper::slug($label), $labels);

	  $seriesMap = [
		'Neuinfektionen (seit letzter Aktualisierung)' => 'cases_24h',
		'Neuinfektionen (24 Stunden)' => 'cases_24h',
		'Neuinfektionen (7 Tage)' => 'cases_7d',
	  ];

	  foreach ($series as $thisSeries) {
		$data_key = $seriesMap[$thisSeries->name];
		$data = $thisSeries->data;

		foreach (range(start: 0, end: sizeof($slugs) - 1) as $index) {
		  $thisInhabitants = $inhabitants[$slugs[$index]];

		  $incidence = $data[$index] * (100_000 / $thisInhabitants);
		  $incidence_key = ($data_key === 'cases_24h') ? 'incidence_24h' : 'incidence_7d';

		  $output[$slugs[$index]]['municipality'] = $labels[$index];
		  $output[$slugs[$index]]['slug'] = $slugs[$index];
		  $output[$slugs[$index]][$data_key] = (int) $data[$index];
		  $output[$slugs[$index]][$incidence_key] = (float) number_format($incidence, thousands_separator: '');
		  $output[$slugs[$index]]['inhabitants'] = (int) $thisInhabitants;
		}
	  }
	}

	return $output;
  }
}
