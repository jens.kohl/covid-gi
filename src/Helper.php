<?php

namespace App;

class Helper {
  public static function slug(string $string): string {
    $string = mb_strtolower($string);
	
    $map = [
      'ä' => 'ae',
      'ö' => 'oe',
      'ü'	=> 'ue',
      'ß' => 'ss',
      '/' => '-',
      "\\" => '-',
      ' ' => '-',
      '(' => '',
      ')' => '',
    ];
    
    $string = str_replace(array_keys($map), array_values($map), $string);
    
    return $string;
  }
}