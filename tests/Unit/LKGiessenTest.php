<?php

it('can download SARS-Cov2 casses data from LK Giessen')
    ->expect((new App\LKGiessen)->getWebpageContent())
    ->not()->toBeEmpty();

it('has stats for 18 municipalities')
    ->expect((new App\LKGiessen)->getCasesAndIncidencePerMunicipality())
    ->toHaveCount(18);

it('has a municipality, cases_(24h|7d), incidence_(24h|7d), inhabitants and slug key for each municipality')
    ->expect((new App\LKGiessen)->getCasesAndIncidencePerMunicipality())
    ->each()
    ->toHaveKeys(['municipality', 'cases_7d', 'cases_7d', 'incidence_24h', 'incidence_7d', 'inhabitants', 'slug']);

it('caches downloaded webpage content')
    ->expect((new App\LKGiessen)->getWebpageContent())
    ->not()->toBeEmpty();

test('that each municipality key is a string')
    ->expect((new App\LKGiessen)->getCasesAndIncidencePerMunicipality())
    ->each(fn($municipality) => $municipality->municipality->toBeString());

test('that each slug key is a string')
    ->expect((new App\LKGiessen)->getCasesAndIncidencePerMunicipality())
    ->each(fn($municipality) => $municipality->slug->toBeString());

test('that each incidence_7d key is numeric')
    ->expect((new App\LKGiessen)->getCasesAndIncidencePerMunicipality())
    ->each(fn($municipality) => $municipality->incidence_7d->toBeNumeric()->toBeGreaterThanOrEqual(0));

test('that each incidence_24h key is numeric')
    ->expect((new App\LKGiessen)->getCasesAndIncidencePerMunicipality())
    ->each(fn($municipality) => $municipality->incidence_24h->toBeNumeric()->toBeGreaterThanOrEqual(0));

test('that each cases_24h key is an integer')
    ->expect((new App\LKGiessen)->getCasesAndIncidencePerMunicipality())
    ->each(fn($municipality) => $municipality->cases_24h->toBeInt()->toBeGreaterThanOrEqual(0));

test('that each cases_7d key is an integer')
    ->expect((new App\LKGiessen)->getCasesAndIncidencePerMunicipality())
    ->each(fn($municipality) => $municipality->cases_7d->toBeInt()->toBeGreaterThanOrEqual(0));

test('that each inhabitants key is an integer and greater than 3500')
    ->expect((new App\LKGiessen)->getCasesAndIncidencePerMunicipality())
    ->each(fn($municipality) => $municipality->inhabitants->toBeInt()->toBeGreaterThanOrEqual(3500));
