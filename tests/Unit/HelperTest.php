<?php

it('does not touch strings without umlauts')
  ->expect(App\Helper::slug('string-without-umlauts'))
  ->toBe('string-without-umlauts');

it('converts multiple umlauts')
  ->expect(App\Helper::slug('Ümläütß'))
  ->toBe('uemlaeuetss');

it('converts slahes to hyphens')
  ->expect(App\Helper::slug('/\\'))
  ->toBe('--');

it('converts spaces to hyphens')
  ->expect(App\Helper::slug(' '))
  ->toBe('-');

it('removes braces')
  ->expect(App\Helper::slug('()'))
  ->toBe('');
