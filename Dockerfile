FROM php:8.1-alpine

RUN mkdir -p /app /app/public /app/resources && apk add --no-cache supervisor
WORKDIR /app
COPY app /app/app/
COPY src /app/src/
COPY vendor /app/vendor/
COPY resources/inhabitants.json /app/resources/inhabitants.json
COPY resources/supervisord.conf /etc/supervisord.conf
COPY resources/generate-json /etc/periodic/15min/
RUN chmod +x /etc/periodic/15min/generate-json

EXPOSE 80

#CMD php -S 0.0.0.0:80 /app/index.php
CMD supervisord -c /etc/supervisord.conf
