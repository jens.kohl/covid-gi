Bevölkerungszahlen
Stand: 31. Dezember 2020

Allendorf (Lumda) 3975, Biebertal 10.055, Buseck 13.042, Fernwald 7097, Gießen 88.781, Grünberg 13.860, Heuchelheim 7934, Hungen 12.748, Langgöns 11.685, Laubach 9769, Lich 14.059, Linden 13.138, Lollar 10.447, Pohlheim 18.469, Rabenau 5097, Reiskirchen 10.682, Staufenberg 8528 und Wettenberg 12.667

https://www.biebertal.de/infos-tipps/gemeindedaten/zahlen-daten-fakten/ - Zahlen aus der Gemeinde Biebertal
