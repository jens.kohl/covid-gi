<?php
declare(strict_types=1);

require __DIR__ . '/../vendor/autoload.php';

$lkgi = new App\LKGiessen();
$output = [
    'municipalities' => $lkgi->getCasesAndIncidencePerMunicipality()
];

header('Content-Type: application/json');
$json = json_encode($output, JSON_PRETTY_PRINT);

if (!file_exists(__DIR__ . '/../public')) {
    mkdir(__DIR__.'/../public');
}

file_put_contents(__DIR__ . '/../public/corona.json', $json);
